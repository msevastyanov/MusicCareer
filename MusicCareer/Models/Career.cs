﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Models
{
    public class Career : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public List<Album> Albums { get; set; }
        public DateTime CurrentDate { get; set; }
        public int Subscribers { get; set; }
        public int Hype { get; set; }
        public int Energy { get; set; }

        public void NextWeek()
        {
            CurrentDate = CurrentDate.AddDays(7);
            Energy += 20;
            if (Energy > 100)
                Energy = 100;
        }

        public void WriteTrack() {
            Energy -= 30;
        }

        public void ReleaseAlbum() {
            Energy -= 70;
        }
    }
}
