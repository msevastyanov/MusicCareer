﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Models
{
    public abstract class MusicUnit : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public bool IsReleased { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public Career Career { get; set; }
    }
}
