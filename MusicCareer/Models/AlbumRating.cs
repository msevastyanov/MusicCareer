﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Models
{
    public class AlbumRating : BaseEntity
    {
        public Reviewer Reviewer { get; set; }
        public Album Album { get; set; }
        public int Rating { get; set; }
    }
}
