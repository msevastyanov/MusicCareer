﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Models
{
    public class Album : MusicUnit
    { 
        public List<Track> Tracks { get; set; }
        [Required]
        public string Type { get; set; }
        public int Rating { get; set; }
    }
}
