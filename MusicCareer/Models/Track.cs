﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Models
{
    public class Track : MusicUnit
    {
        public int? ChartsPeek { get; set; }
        public int? ChartsPosition { get; set; }
        public int? LastChartsPosition { get; set; }
        public Album Album { get; set; }
        public bool IsSingle { get; set; }
        public string Theme { get; set; }
        public string Style { get; set; }
        public DateTime RecordedDate { get; set; }
    }
}
