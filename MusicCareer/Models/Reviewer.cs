﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Models
{
    public class Reviewer : BaseEntity
    {
        public string Name { get; set; }
    }
}
