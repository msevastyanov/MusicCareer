﻿using Microsoft.AspNetCore.Mvc;
using MusicCareer.Services;
using MusicCareer.ViewModels;
using MusicCareer.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Controllers
{
    [Route("api/[controller]")]
    public class TracksController : Controller
    {
        private readonly ITrackService _trackService;

        public TracksController(ITrackService trackService)
        {
            _trackService = trackService;
        }

        // GET api/tracks/career/{careerId}
        [HttpGet("career/{careerId}")]
        public async Task<IActionResult> GetAllTracks(Guid careerId)
        {
            return Json(await _trackService.GetAllTracks(careerId));
        }

        // GET api/tracks/career/{careerId}/unreleased
        [HttpGet("career/{careerId}/unreleased")]
        public async Task<IActionResult> GetUnreleasedTracks(Guid careerId)
        {
            return Json(await _trackService.GetUnreleasedTracks(careerId));
        }

        // GET api/tracks/album/{albumId}
        [HttpGet("album/{albumId}")]
        public async Task<IActionResult> GetTracksByAlbum(Guid albumId)
        {
            return Json(await _trackService.GetTracksByAlbum(albumId));
        }

        // POST api/tracks/{careerId}
        [HttpPost("{careerId}")]
        public async Task<IActionResult> CreateTrack(Guid careerId, [FromBody] TrackCreateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _trackService.CreateTrack(model, careerId);

            return result.ToJsonResult();
        }

        // POST api/tracks/{id}/remove
        [HttpPost("{id}/remove")]
        public async Task<IActionResult> RemoveTrack(Guid id)
        {
            var result = await _trackService.RemoveTrack(id);

            return result.ToJsonResult();
        }

        // POST api/tracks/{id}/release/{careerId}
        [HttpPost("{id}/release/{careerId}")]
        public async Task<IActionResult> ReleaseTrack(Guid id, Guid careerId)
        {
            var result = await _trackService.ReleaseTrack(careerId, id);

            return result.ToJsonResult();
        }
    }
}
