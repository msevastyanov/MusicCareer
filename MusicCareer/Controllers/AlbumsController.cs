﻿using Microsoft.AspNetCore.Mvc;
using MusicCareer.Services;
using MusicCareer.ViewModels;
using MusicCareer.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Controllers
{
    [Route("api/[controller]")]
    public class AlbumsController : Controller
    {
        private readonly IAlbumService _albumService;

        public AlbumsController(IAlbumService albumService)
        {
            _albumService = albumService;
        }

        // GET api/albums/{careerId}/released
        [HttpGet("{careerId}/released")]
        public async Task<IActionResult> GetReleasedAlbums(Guid careerId)
        {
            return Json(await _albumService.GetAlbums(careerId, true));
        }

        // GET api/albums/{careerId}/unreleased
        [HttpGet("{careerId}/unreleased")]
        public async Task<IActionResult> GetUnreleasedAlbums(Guid careerId)
        {
            return Json(await _albumService.GetAlbums(careerId, false));
        }

        // POST api/albums/{careerId}
        [HttpPost("{careerId}")]
        public async Task<IActionResult> CreateAlbum(Guid careerId, [FromBody] AlbumCreateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _albumService.CreateAlbum(model, careerId);

            return result.ToJsonResult();
        }

        // PUT api/albums/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAlbum(Guid id, [FromBody] AlbumUpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(new { status = "fail", message = "Bad request" });

            var result = await _albumService.UpdateAlbum(id, model);

            return result.ToJsonResult();
        }

        // POST api/albums/{id}/remove
        [HttpPost("{id}/remove")]
        public async Task<IActionResult> RemoveAlbum(Guid id)
        {
            var result = await _albumService.RemoveAlbum(id);

            return result.ToJsonResult();
        }

        // POST api/albums/{id}/release/{careerId}
        [HttpPost("{id}/release/{careerId}")]
        public async Task<IActionResult> ReleaseAlbum(Guid id, Guid careerId)
        {
            var result = await _albumService.ReleaseAlbum(careerId, id);

            return result.ToJsonResult();
        }

        // POST api/albums/{albumId}/track/{trackId}
        [HttpPost("{albumId}/track/{trackId}")]
        public async Task<IActionResult> AddTrackToAlbum(Guid albumId, Guid trackId)
        {
            var result = await _albumService.AddTrackToAlbum(albumId, trackId);

            return result.ToJsonResult();
        }

        // POST api/albums/{albumId}/track/{trackId}/remove
        [HttpPost("{albumId}/track/{trackId}/remove")]
        public async Task<IActionResult> RemoveTrackFromAlbum(Guid albumId, Guid trackId)
        {
            var result = await _albumService.RemoveTrackFromAlbum(albumId, trackId);

            return result.ToJsonResult();
        }
    }
}
