﻿using Microsoft.AspNetCore.Mvc;
using MusicCareer.Services;
using MusicCareer.ViewModels;
using MusicCareer.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Controllers
{
    [Route("api/[controller]")]
    public class CareersController : Controller
    {
        private readonly ICareerService _careerService;

        public CareersController(ICareerService careerService)
        {
            _careerService = careerService;
        }

        // GET api/careers
        [HttpGet]
        public async Task<IActionResult> GetCareers()
        {
            return Json(await _careerService.GetCareers());
        }

        // GET api/careers/{careerId}
        [HttpGet("{careerId}")]
        public async Task<IActionResult> GetCareer(Guid careerId)
        {
            return Json(await _careerService.GetCareer(careerId));
        }

        // POST api/careers
        [HttpPost]
        public async Task<IActionResult> CreateCareer([FromBody] CareerCreateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _careerService.CreateCareer(model);

            return result.ToJsonResult();
        }

        // PUT api/careers/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCareer(Guid id, [FromBody] CareerUpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(new { status = "fail", message = "Bad request" });

            var result = await _careerService.UpdateCareer(id, model);

            return result.ToJsonResult();
        }

        // POST api/careers/{id}/remove
        [HttpPost("{id}/remove")]
        public async Task<IActionResult> RemoveCareer(Guid id)
        {
            var result = await _careerService.RemoveCareer(id);

            return result.ToJsonResult();
        }

        // POST api/careers/{id}/next
        [HttpPost("{id}/next")]
        public async Task<IActionResult> NextWeek(Guid id)
        {
            var result = await _careerService.NextWeek(id);

            return result.ToJsonResult();
        }
    }
}
