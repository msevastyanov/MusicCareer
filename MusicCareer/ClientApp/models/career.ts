export default class Career {
    id: string;
    name: string;
    subscribers: number;
    hype: number;
    energy: number;
    currentDate: string;

    constructor (obj: any) {
        obj = obj || {};
        this.id = obj.id || '';
        this.name = obj.name || '';
        this.subscribers = obj.subscribers || 0;
        this.hype = obj.hype || 0;
        this.energy = obj.energy || 0;
        this.currentDate = obj.currentDate || 0;
    }
}