import axios from 'axios'
import { APP_URL } from '../config'

export class TracksApi {
    async getAllTracks(careerId: string) {
        let response = await axios.get(`${APP_URL}api/tracks/career/${careerId}`);
        return response.data
    }

    async getUnreleasedTracks(careerId: string) {
        let response = await axios.get(`${APP_URL}api/tracks/career/${careerId}/unreleased`);
        console.log('response', response)
        return response.data
    }

    async getTracksByAlbum(albumId: string) {
        let response = await axios.get(`${APP_URL}api/tracks/album/${albumId}`);
        return response.data
    }

    async addTrack(careerId: string, track: any) {
        let response = await axios.post(`${APP_URL}api/tracks/${careerId}`, track);
        return response.data
    }

    async removeTrack(id: string) {
        let response = await axios.post(`${APP_URL}api/tracks/${id}/remove`);
        return response.data.status === 'success'
    }

    async releaseTrack(careerId: string, id: string) {
        let response = await axios.post(`${APP_URL}api/tracks/${id}/release/${careerId}`);
        return response.data.status === 'success'
    }
}
