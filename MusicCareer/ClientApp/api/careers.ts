﻿import axios from 'axios'
import { APP_URL } from '../config'

export class CareersApi {
    async getCareers() {
        let response = await axios.get(`${APP_URL}api/careers`);
        return response.data
    }

    async getCareer(id: string) {
        let response = await axios.get(`${APP_URL}api/careers/${id}`);
        return response.data
    }

    async addCareer(career: any) {
        let response = await axios.post(`${APP_URL}api/careers`, career);
        return response.data
    }

    async updateCareer(career: any) {
        let response = await axios.put(`${APP_URL}api/careers/${career.id}`, career);
        return response.data
    }

    async removeCareer(id: string) {
        let response = await axios.post(`${APP_URL}api/careers/${id}/remove`);
        return response.data.status === 'success'
    }

    async nextWeek(id: string) {
        let response = await axios.post(`${APP_URL}api/careers/${id}/next`);
        return response.data
    }
}
