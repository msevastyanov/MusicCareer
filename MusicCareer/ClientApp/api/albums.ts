import axios from 'axios'
import { APP_URL } from '../config'

export class AlbumsApi {
    async getReleasedAlbums(careerId: string) {
        let response = await axios.get(`${APP_URL}api/albums/${careerId}/released`);
        console.log('responseresponse', response)
        return response.data
    }

    async getUnreleasedAlbums(careerId: string) {
        let response = await axios.get(`${APP_URL}api/albums/${careerId}/unreleased`);
        return response.data
    }

    async addAlbum(careerId: string, album: any) {
        let response = await axios.post(`${APP_URL}api/albums/${careerId}`, album);
        return response.data
    }

    async removeAlbum(id: string) {
        let response = await axios.post(`${APP_URL}api/albums/${id}/remove`);
        return response.data.status === 'success'
    }

    async releaseAlbum(careerId: string, id: string) {
        let response = await axios.post(`${APP_URL}api/albums/${id}/release/${careerId}`);
        return response.data.status === 'success'
    }

    async addTrackToAlbum(albumId: string, trackId: string) {
        let response = await axios.post(`${APP_URL}api/albums/${albumId}/track/${trackId}`);
        console.log('111response222', response)
        return response.data
    }

    async removeTrackFromAlbum(albumId: string, trackId: string) {
        let response = await axios.post(`${APP_URL}api/albums/${albumId}/track/${trackId}/remove`);
        return response.data
    }
}
