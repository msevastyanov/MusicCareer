interface State {
    unreleasedAlbums: any[];
    releasedAlbums: any[];
}

export default function careers(state: State = {
    unreleasedAlbums: [],
    releasedAlbums: [],
}, action: any) {
    switch (action.type) {
        case 'SET_UNRELEASED_ALBUMS':
            return {
                ...state,
                unreleasedAlbums: action.albums
            };
        case 'SET_RELEASED_ALBUMS':
            return {
                ...state,
                releasedAlbums: action.albums
            };
        case 'ADD_ALBUM':
            return {
                ...state,
                unreleasedAlbums: [...state.unreleasedAlbums, action.album]
            };
        case 'UPDATE_ALBUM':
            return {
                ...state,
                unreleasedAlbums: state.unreleasedAlbums.map(
                    (album) => (album.id === action.album.id ? action.album : album)
                )
            };
        case 'REMOVE_ALBUM':
            return {
                ...state,
                unreleasedAlbums: state.unreleasedAlbums.filter((album) => album.id !== action.id)
            };
        case 'RELEASE_ALBUM':
            return {
                ...state,
                unreleasedAlbums: state.unreleasedAlbums.filter((album) => album.id !== action.id)
            };
        default:
            return state;
    }
}
