﻿interface State {
    careers: any[];
    career: any;
}

export default function careers(state: State = {
    careers: [],
    career: null
}, action: any) {
    switch (action.type) {
        case 'SET_CAREERS':
            // state.careers = action.careers;
            return {
                ...state,
                careers: action.careers
            };
        case 'SET_CAREER':
            return {
                ...state,
                career: action.career
            };
        case 'ADD_CAREER':
            return {
                ...state,
                careers: [...state.careers, action.career]
            };
        case 'UPDATE_CAREER':
            return {
                ...state,
                careers: state.careers.map(
                    (career) => (career.id === action.career.id ? action.career : career)
                ),
                career: action.career
            };
        case 'REMOVE_CAREER':
            return {
                ...state,
                careers: state.careers.filter((career) => career.id !== action.id)
            };
        default:
            return state;
    }
}
