﻿import { combineReducers } from 'redux';
import careers from './careers';
import tracks from './tracks';
import albums from './albums';

export default combineReducers({
    careers,
    tracks,
    albums,
});
