interface State {
    unreleasedTracks: any[];
    albumTracks: any[];
}

export default function careers(state: State = {
    unreleasedTracks: [],
    albumTracks: [],
}, action: any) {
    switch (action.type) {
        case 'SET_UNRELEASED_TRACKS':
            return {
                ...state,
                unreleasedTracks: action.tracks
            };
        case 'SET_ALBUM_TRACKS':
            return {
                ...state,
                albumTracks: action.tracks
            };
        case 'ADD_TRACK':
            return {
                ...state,
                unreleasedTracks: [...state.unreleasedTracks, action.track]
            };
        case 'REMOVE_TRACK':
            return {
                ...state,
                unreleasedTracks: state.unreleasedTracks.filter((track) => track.id !== action.id)
            };
        case 'RELEASE_TRACK':
            return {
                ...state,
                unreleasedTracks: state.unreleasedTracks.filter((track) => track.id !== action.id)
            };
        default:
            return state;
    }
}
