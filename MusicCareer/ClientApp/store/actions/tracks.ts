import { TracksApi } from '../../api';

const api = new TracksApi();

const setUnreleasedTracks = (tracks: any[]) => ({
    type: 'SET_UNRELEASED_TRACKS',
    tracks,
});

export const getUnreleasedTracks = (careerId: string) => async (dispatch: any) => {
    console.log('sss', careerId)
    dispatch(setUnreleasedTracks(await api.getUnreleasedTracks(careerId)));
};

const setTracksByAlbum = (tracks: any[]) => ({
    type: 'SET_ALBUM_TRACKS',
    tracks,
});

export const getTracksByAlbum = (albumId: string) => async (dispatch: any) => {
    dispatch(setTracksByAlbum(await api.getTracksByAlbum(albumId)));
};

const _addTrack = (track: any) => ({
    type: 'ADD_TRACK',
    track
});

export const addTrack = (careerId: string, data: any) => async (dispatch: any) => {
    console.log('careerIdcareerIdcareerId', careerId)
    console.log('datadatadata', data)
    let res = await api.addTrack(careerId, data);
    if (res.status === "success")
        dispatch(_addTrack(res.data));
    return res;
};

const _removeTrack = (id: string) => ({
    type: 'REMOVE_TRACK',
    id,
});

export const removeTrack = (id: string) => async (dispatch: any) => {
    await api.removeTrack(id);
    dispatch(_removeTrack(id));
};

const _releaseTrack = (id: string) => ({
    type: 'RELEASE_TRACK',
    id,
});

export const releaseTrack = (careerId: string, id: string) => async (dispatch: any) => {
    await api.releaseTrack(careerId, id);
    dispatch(_releaseTrack(id));
};