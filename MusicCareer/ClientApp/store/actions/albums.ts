import { AlbumsApi } from '../../api';

const api = new AlbumsApi();

const setReleasedAlbums = (albums: any[]) => ({
    type: 'SET_RELEASED_ALBUMS',
    albums,
});

export const getReleasedAlbums = (careerId: string) => async (dispatch: any) => {
    dispatch(setReleasedAlbums(await api.getReleasedAlbums(careerId)));
};

const setUnreleasedAlbums = (albums: any[]) => ({
    type: 'SET_UNRELEASED_ALBUMS',
    albums,
});

export const getUnreleasedAlbums = (careerId: string) => async (dispatch: any) => {
    dispatch(setUnreleasedAlbums(await api.getUnreleasedAlbums(careerId)));
};

const _addAlbum = (album: any) => ({
    type: 'ADD_ALBUM',
    album
});

export const addAlbum = (careerId: string, data: any) => async (dispatch: any) => {
    let res = await api.addAlbum(careerId, data);
    if (res.status === "success")
        dispatch(_addAlbum(res.data));
    return res;
};

const _updateAlbum = (album: any) => ({
    type: 'UPDATE_ALBUM',
    album,
});


const _removeAlbum = (id: string) => ({
    type: 'REMOVE_ALBUM',
    id,
});

export const removeAlbum = (id: string) => async (dispatch: any) => {
    await api.removeAlbum(id);
    dispatch(_removeAlbum(id));
};

const _releaseAlbum = (id: string) => ({
    type: 'RELEASE_ALBUM',
    id,
});

export const releaseAlbum = (careerId: string, id: string) => async (dispatch: any) => {
    await api.releaseAlbum(careerId, id);
    dispatch(_releaseAlbum(id));
};

export const addTrackToAlbum = (albumId: string, trackId: string) => async (dispatch: any) => {
    dispatch(_updateAlbum(await api.addTrackToAlbum(albumId, trackId)));
};

export const removeTrackFromAlbum = (albumId: string, trackId: string) => async (dispatch: any) => {
    dispatch(_updateAlbum(await api.removeTrackFromAlbum(albumId, trackId)));
};