﻿import { CareersApi } from '../../api';

const api = new CareersApi();

const setCareers = (careers: any[]) => ({
    type: 'SET_CAREERS',
    careers,
});

export const getCareers = () => async (dispatch: any) => {
    dispatch(setCareers(await api.getCareers()));
};

const setCareer = (career: any) => ({
    type: 'SET_CAREER',
    career,
});

export const getCareer = (id: string) => async (dispatch: any) => {
    dispatch(setCareer(await api.getCareer(id)));
};

const _addCareer = (career: any) => ({
    type: 'ADD_CAREER',
    career
});

export const addCareer = (data: any) => async (dispatch: any) => {
    let res = await api.addCareer(data);
    if (res.status === "success")
        dispatch(_addCareer(res.data));
    return res;
};

const _updateCareer = (career: any) => ({
    type: 'UPDATE_CAREER',
    career
});

export const updateCareer = (data: any) => async (dispatch: any) => {
    dispatch(_updateCareer(await api.updateCareer(data)));
};

const _removeCareer = (id: string) => ({
    type: 'REMOVE_ALBUM',
    id,
});

export const removeCareer = (id: string) => async (dispatch: any) => {
    await api.removeCareer(id);
    dispatch(_removeCareer(id));
};

export const nextWeek = (id: string) => async (dispatch: any) => {
    let res = await api.nextWeek(id);
    if (res.status === "success")
        dispatch(_updateCareer(res.data));
};