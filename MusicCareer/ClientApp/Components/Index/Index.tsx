﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

interface Props {
    getCareers: Function,
    careers: any[],
}

class Home extends React.Component<RouteComponentProps<{}> & Props, {}> {
    componentDidMount() {
        
    }

    public render() {
        return <div>
            Music Career Simulator
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
    
});

export default connect(
    mapStateToProps,
    { }
)(Home);
