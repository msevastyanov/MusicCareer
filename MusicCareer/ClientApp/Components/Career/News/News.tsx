import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';

import styles from "./News.scss"

interface Props {
    
}

interface State {
    
}

class News extends React.Component<RouteComponentProps<{}> & Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            
        }
    }

    componentDidMount() {
        
    }

    public render() {
        return <div>
            career news
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
   
});

export default connect(
    mapStateToProps,
    { }
)(News);
