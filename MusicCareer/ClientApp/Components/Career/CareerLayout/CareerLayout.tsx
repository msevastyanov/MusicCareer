import * as React from 'react';
import { connect } from 'react-redux';
import Sidebar from "../Sidebar/Sidebar";
import Topbar from "../Topbar/Topbar";
import CurrentDate from "../CurrentDate/CurrentDate";

import mainStyles from "../../../styles/main.scss";
import styles from "./CareerLayout.scss"

interface Props {
    children?: React.ReactNode;
}

interface State {
}

class CareerLayout extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {

    }

    public render() {
        return <div>
            <div className="row">
                <div className="col-md-3">
                    <Sidebar />
                </div>
                <div className="col-md-9">
                    <div className="row">
                        <div className="col-md-9">
                            <Topbar />
                        </div>
                        <div className="col-md-3">
                            <CurrentDate />
                        </div>
                    </div>
                    <div className={`${mainStyles.card} ${styles.layout}`}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        </div>;
    }
}

const mapStateToProps = (state: any) => ({

});

export default connect(
    mapStateToProps,
    {}
)(CareerLayout);
