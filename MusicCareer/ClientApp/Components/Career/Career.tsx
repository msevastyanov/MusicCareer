﻿import * as React from 'react';
import { Switch, Route } from 'react-router';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import { getCareer, getReleasedAlbums } from '../../store/actions';
import CareerModel from "../../models/career";
import CareerLayout from "./CareerLayout/CareerLayout";
import News from "./News/News";
import Discography from "./Discography/Discography";
import Achievements from "./Achievements/Achievements";
import Fabric from "./Fabric/Fabric";

import styles from "./Career.scss"

interface Props {
    getCareer: Function,
    getReleasedAlbums: Function,
    career: CareerModel;
    match: any;
}

interface State {
}

class Career extends React.Component<RouteComponentProps<{}> & Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        this.props.getCareer(this.props.match.params.id)
        this.props.getReleasedAlbums(this.props.match.params.id)
    }

    public render() {
        return <CareerLayout>
            <Switch>
                <Route exact path="/career/:id" component={News} />
                <Route path="/career/:id/discography" component={Discography} />
                <Route path="/career/:id/achievements" component={Achievements} />
                <Route path="/career/:id/fabric" component={Fabric} />
            </Switch>
        </CareerLayout>;
    }
}

const mapStateToProps = (state: any) => ({

});

export default connect(
    mapStateToProps,
    { getCareer, getReleasedAlbums }
)(Career);
