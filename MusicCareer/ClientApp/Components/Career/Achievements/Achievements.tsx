import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';

import styles from "./Achievements.scss"

interface Props {
    
}

interface State {
    
}

class Achievements extends React.Component<RouteComponentProps<{}> & Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            
        }
    }

    componentDidMount() {
        
    }

    public render() {
        return <div>
            career achievements
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
   
});

export default connect(
    mapStateToProps,
    { }
)(Achievements);
