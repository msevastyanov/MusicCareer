import * as React from 'react';
import { connect } from 'react-redux';
import moment from "moment"
import CareerModel from "../../../models/career";

import { nextWeek } from '../../../store/actions';

import mainStyles from "../../../styles/main.scss";
import styles from "./CurrentDate.scss";

interface Props {
    career: any;
    nextWeek: Function;
}

interface State {

}

class CurrentDate extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
    }

    render() {
        let career: any = new CareerModel(this.props.career);

        return <div className={`${mainStyles.card} ${styles.currentDate}`}>
            <span className={styles.date}>
                {moment(career.currentDate).format("DD.MM.YYYY")}
            </span>
            <a onClick={() => { this.props.nextWeek(career.id) }} className={styles.nextWeek} title="Следующая неделя">
                <i className="fas fa-caret-right"></i>
            </a>
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
    career: state.careers.career
});

export default connect(
    mapStateToProps,
    { nextWeek }
)(CurrentDate);