import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import AlbumsFabric from "./AlbumsFabric/AlbumsFabric"
import TracksFabric from "./TracksFabric/TracksFabric"

import styles from "./Fabric.scss"

interface Props {
    
}

interface State {
    
}

class Fabric extends React.Component<RouteComponentProps<{}> & Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            
        }
    }

    componentDidMount() {
        
    }

    public render() {
        return <React.Fragment>
            <AlbumsFabric />
            <br />
            <TracksFabric />
        </React.Fragment>;
    }
}

const mapStateToProps = (state: any) => ({
   
});

export default connect(
    mapStateToProps,
    { }
)(Fabric);
