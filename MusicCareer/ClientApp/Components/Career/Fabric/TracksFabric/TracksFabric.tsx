import * as React from 'react';
import { connect } from 'react-redux';
import TracksList from "./TracksList/TracksList"
import Modal from "../../../Modal/Modal"
import TrackForm from "./TrackForm/TrackForm"

import { getUnreleasedTracks } from '../../../../store/actions';

import mainStyles from "../../../../styles/main.scss";
import styles from "./TracksFabric.scss";

interface Props {
    getUnreleasedTracks: Function;
    tracks: any[];
    career: any;
}

interface State {
    trackModal: boolean;
}

class TracksFabric extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            trackModal: false,
        }
    }

    componentDidMount() {
        if (this.props.career && this.props.career.id)
            this.props.getUnreleasedTracks(this.props.career.id)
    }

    componentDidUpdate(prevProps: any) {
        if (!prevProps.career || (this.props.career && this.props.career.id && this.props.career.id !== prevProps.career.id))
            this.props.getUnreleasedTracks(this.props.career.id)
    }

    render() {
        let tracks = this.props.tracks;

        return <div>
            <div className="row">
                <div className="col-md-9">
                    <h1 className={mainStyles.title}>
                        Треки
                    </h1>
                </div>
                <div className={`col-md-3 ${mainStyles.alignRight}`}>
                    <a onClick={() => { this.setState({ trackModal: true }) }} className={`btn btn-primary ${mainStyles.btn} ${mainStyles.btnPrimary}`}>
                        Создать трек
                    </a>
                </div>
            </div>
            <TracksList tracks={tracks} />
            {this.state.trackModal &&
                <Modal title="Новый трек" close={() => { this.setState({ trackModal: false }) }}>
                    <TrackForm close={() => { this.setState({ trackModal: false }) }} />
                </Modal>}
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
    career: state.careers.career,
    tracks: state.tracks.unreleasedTracks.filter((track: any) => !track.album),
});

export default connect(
    mapStateToProps,
    { getUnreleasedTracks }
)(TracksFabric);