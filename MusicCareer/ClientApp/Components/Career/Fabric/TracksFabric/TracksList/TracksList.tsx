import * as React from 'react';
import { connect } from 'react-redux';
import moment from "moment"

import { removeTrack } from '../../../../../store/actions';

import mainStyles from "../../../../../styles/main.scss";
import styles from "./TracksList.scss";

interface Props {
    tracks: any[];
    removeTrack: Function;
}

interface State {

}

class TracksList extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
    }

    render() {
        let tracks = this.props.tracks;

        return <div className={styles.list}>
            <table className={`table ${mainStyles.table}`}>
                <tbody>
                    {tracks.map((track: any) => {
                        return (
                            <tr key={track.id}>
                                <td className={styles.trackDate}>
                                    {moment(track.recordedDate).format("DD.MM.YYYY")}
                                </td>
                                <td>
                                    {track.name}
                                </td>
                                <td>
                                    {track.theme}
                                </td>
                                <td className={mainStyles.alignRight}>
                                    <a onClick={() => { this.props.removeTrack(track.id) }} className={`btn btn-secondary ${mainStyles.btn} ${mainStyles.btnSecondary}`}>
                                        <i className="fas fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>;
    }
}

const mapStateToProps = (state: any) => ({

});

export default connect(
    mapStateToProps,
    { removeTrack }
)(TracksList);