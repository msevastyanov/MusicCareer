import * as React from 'react';
import { connect } from 'react-redux';

import { addTrack, getCareer } from '../../../../../store/actions';

import mainStyles from "../../../../../styles/main.scss";

interface Props {
    addTrack: Function;
    getCareer: Function;
    close: Function;
    career: any;
}

interface State {
    name: string;
    theme: string;
    error: string;
}

class TrackForm extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            theme: 'me',
            error: '',
        }
    }

    componentDidMount() {

    }

    onChange = (e: any) => {
        this.setState({ [e.currentTarget.name]: e.target.value } as State);
        this.setState({ error: null })
    };

    onSubmit = (e: any) => {
        e.preventDefault();
        let careerId = this.props.career.id;
        this.props.addTrack(careerId, this.state).then((res: any) => {
            if (res.status === "success") {
                this.props.getCareer(careerId)
                this.props.close()
            } else {
                this.setState({ error: res.message })
            }
            res.status === "success" ? this.props.close() : this.setState({ error: res.message })
        });
    };

    render() {
        let { name, theme, error } = this.state;
        let themes = [{
            name: 'me',
            alias: 'О себе'
        }, {
            name: 'story',
            alias: 'Рассказ'
        }, {
            name: 'love',
            alias: 'Любовь'
        }, {
            name: 'sex',
            alias: 'Секс'
        }, {
            name: 'death',
            alias: 'Смерть'
        }, {
            name: 'money',
            alias: 'Деньги'
        }, {
            name: 'social',
            alias: 'Общество'
        }, {
            name: 'politics',
            alias: 'Политика'
        }, {
            name: 'drugs',
            alias: 'Наркотики'
        }, {
            name: 'motivation',
            alias: 'Мотивация'
        }, {
            name: 'philosophy',
            alias: 'Философия'
        }, {
            name: 'music',
            alias: 'Музыка'
        }, {
            name: 'family',
            alias: 'Семья'
        }, {
            name: 'nostalgia',
            alias: 'Ностальгия'
        }];

        return <form className="form">
            <div className={`form-group ${mainStyles.formGroup}`}>
                <input value={name} onChange={this.onChange} type="text" name="name" className={`form-control ${mainStyles.formControl}`} placeholder="Название" />
            </div>
            <div className={`form-group ${mainStyles.formGroup}`}>
                <select value={theme} onChange={this.onChange} name="theme" className={`form-control ${mainStyles.formControl}`} placeholder="Тематика">
                    {themes.map((theme: any) => {
                        return (
                            <option value={theme.name}>
                                {theme.alias}
                            </option>
                        )
                    })}
                </select>
            </div>
            {error && <p className={mainStyles.formError}>{error}</p>}
            <div className={mainStyles.formActions}>
                <a onClick={this.onSubmit} className={`btn btn-primary ${mainStyles.btn} ${mainStyles.btnPrimary}`}>
                    Добавить
                </a>
            </div>
        </form>;
    }
}

const mapStateToProps = (state: any) => ({
    career: state.careers.career,
});

export default connect(
    mapStateToProps,
    { addTrack, getCareer }
)(TrackForm);
