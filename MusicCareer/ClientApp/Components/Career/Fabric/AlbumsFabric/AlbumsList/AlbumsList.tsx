import * as React from 'react';
import { connect } from 'react-redux';
import moment from "moment"

import { removeAlbum, releaseAlbum, getUnreleasedTracks, getReleasedAlbums } from '../../../../../store/actions';

import mainStyles from "../../../../../styles/main.scss";
import styles from "./AlbumsList.scss";

interface Props {
    albums: any[];
    removeAlbum: Function;
    releaseAlbum: Function;
    selectAlbum: Function;
    getUnreleasedTracks: Function;
    getReleasedAlbums: Function;
    albumId: string;
    careerId: string;
}

interface State {

}

class AlbumsList extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
    }

    releaseAlbum = (id: string) => {
        this.props.releaseAlbum(this.props.careerId, id).then(() => {
            this.props.getUnreleasedTracks(this.props.careerId)
            this.props.getReleasedAlbums(this.props.careerId)
        })
    }

    render() {
        let albums = this.props.albums;

        return <div className={styles.list}>
            <table className={`table ${mainStyles.table}`}>
                <tbody>
                    {albums.map((album: any) => {
                        let minCount = album.type === "LP" ? 10 : 5;

                        return (
                            <tr key={album.id}>
                                <td>
                                    <a onClick={() => { this.props.selectAlbum(album.id) }} className={`${styles.albumLink} ${this.props.albumId === album.id ? styles.selected : ''}`}>
                                        {album.name}
                                    </a>
                                </td>
                                <td>
                                    {album.type}
                                </td>
                                <td>
                                    Tracks: {album.tracks.length}
                                </td>
                                <td className={`${mainStyles.alignRight} ${mainStyles.cellActions}`}>
                                    {album.tracks.length >= minCount && <a onClick={() => { this.releaseAlbum(album.id) }} className={`btn btn-warning ${mainStyles.btn} ${mainStyles.btnWarning}`} title="Релиз">
                                        <i className="fas fa-check"></i>
                                    </a>}
                                    <a onClick={() => { this.props.removeAlbum(album.id) }} className={`btn btn-secondary ${mainStyles.btn} ${mainStyles.btnSecondary}`} title="Удалить">
                                        <i className="fas fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
    careerId: state.careers.career && state.careers.career.id
});

export default connect(
    mapStateToProps,
    { removeAlbum, releaseAlbum, getUnreleasedTracks, getReleasedAlbums }
)(AlbumsList);