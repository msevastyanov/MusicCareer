import * as React from 'react';
import { connect } from 'react-redux';
import moment from "moment"

import { addTrackToAlbum, removeTrackFromAlbum, getUnreleasedTracks } from '../../../../../store/actions';

import mainStyles from "../../../../../styles/main.scss";
import styles from "./AlbumTracks.scss";

interface Props {
    albumId: string;
    tracks: any[];
    unreleasedTracks: any[];
    careerId: string;
    addTrackToAlbum: Function;
    removeTrackFromAlbum: Function;
    getUnreleasedTracks: Function;
}

interface State {
    selectedTrackId: string;
}

class AlbumTracks extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            selectedTrackId: ''
        }
    }

    addTrack = () => {
        this.props.addTrackToAlbum(this.props.albumId, this.state.selectedTrackId).then(() => {
            this.props.getUnreleasedTracks(this.props.careerId)
            this.setState({selectedTrackId: null})
        })
    };

    removeTrack = (trackId: string) => {
        this.props.removeTrackFromAlbum(this.props.albumId, trackId).then(() => {
            this.props.getUnreleasedTracks(this.props.careerId)
        })
    };

    onChange = (e: any) => {
        this.setState({ [e.currentTarget.name]: e.target.value } as State);
    };

    render() {
        let albumId = this.props.albumId;
        let tracks = this.props.tracks;
        let unreleasedTracks = this.props.unreleasedTracks;
        let selectedTrackId = this.state.selectedTrackId;

        if (!selectedTrackId && unreleasedTracks.length)
            this.setState({ selectedTrackId: unreleasedTracks[0].id })

        return <div className={styles.list}>
            {albumId && <div className={styles.selectTrack}>
                <select value={selectedTrackId} onChange={this.onChange} name="selectedTrackId" className={`form-control ${mainStyles.formControl}`} placeholder="Трек">
                    {unreleasedTracks.map((track: any) => {
                        return (
                            <option value={track.id}>
                                {track.name}
                            </option>
                        )
                    })}
                </select>
                <a onClick={() => { this.addTrack() }} className={`btn btn-secondary ${mainStyles.btn} ${mainStyles.btnSecondary}`}>
                    <i className="fas fa-plus"></i>
                </a>
            </div>}
            <table className={`table ${mainStyles.table} ${styles.tracksTable}`}>
                <tbody>
                    {tracks.map((track: any) => {
                        return (
                            <tr key={track.id}>
                                <td className={styles.trackDate}>
                                    {moment(track.recordedDate).format("DD.MM.YYYY")}
                                </td>
                                <td>
                                    {track.name}
                                </td>
                                <td>
                                    {track.theme}
                                </td>
                                <td className={mainStyles.alignRight}>
                                    <a onClick={() => { this.removeTrack(track.id) }} className={`btn btn-secondary ${mainStyles.btn} ${mainStyles.btnSecondary}`}>
                                        <i className="fas fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            {this.props.albumId && !tracks.length && <p className={mainStyles.info}>Нет добавленных треков</p>}
        </div>;
    }
}

const mapStateToProps = (state: any, ownProps: any) => ({
    tracks: state.tracks.unreleasedTracks.filter((track: any) => track.album && track.albumId === ownProps.albumId),
    unreleasedTracks: state.tracks.unreleasedTracks.filter((track: any) => !track.album),
    careerId: state.careers.career && state.careers.career.id
});

export default connect(
    mapStateToProps,
    { addTrackToAlbum, removeTrackFromAlbum, getUnreleasedTracks }
)(AlbumTracks);