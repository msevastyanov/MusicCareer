import * as React from 'react';
import { connect } from 'react-redux';
import AlbumsList from "./AlbumsList/AlbumsList"
import AlbumTracks from "./AlbumTracks/AlbumTracks"
import Modal from "../../../Modal/Modal"
import AlbumForm from "./AlbumForm/AlbumForm"

import { getUnreleasedAlbums } from '../../../../store/actions';

import mainStyles from "../../../../styles/main.scss";
import styles from "./AlbumsFabric.scss";

interface Props {
    getUnreleasedAlbums: Function;
    albums: any[];
    career: any;
}

interface State {
    albumModal: boolean;
    selectedAlbumId: string;
}

class AlbumsFabric extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            albumModal: false,
            selectedAlbumId: '',
        }
    }

    componentDidMount() {
        if (this.props.career && this.props.career.id)
            this.props.getUnreleasedAlbums(this.props.career.id)
    }

    componentDidUpdate(prevProps: any) {
        if (!prevProps.career || (this.props.career && this.props.career.id && this.props.career.id !== prevProps.career.id))
            this.props.getUnreleasedAlbums(this.props.career.id)
    }

    render() {
        let albums = this.props.albums;

        return <div>
            <div className="row">
                <div className="col-md-9">
                    <h1 className={mainStyles.title}>
                        Альбомы
                    </h1>
                </div>
                <div className={`col-md-3 ${mainStyles.alignRight}`}>
                    <a onClick={() => { this.setState({ albumModal: true }) }} className={`btn btn-primary ${mainStyles.btn} ${mainStyles.btnPrimary}`}>
                        Создать альбом
                    </a>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-6">
                    <AlbumsList albums={albums} albumId={this.state.selectedAlbumId} selectAlbum={(id: string) => {this.setState({selectedAlbumId: id})}} />
                </div>
                <div className="col-sm-6">
                    <AlbumTracks albumId={this.state.selectedAlbumId} />
                </div>
            </div>
            {this.state.albumModal &&
                <Modal title="Новый альбом" close={() => { this.setState({ albumModal: false }) }}>
                    <AlbumForm close={() => { this.setState({ albumModal: false }) }} />
                </Modal>}
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
    career: state.careers.career,
    albums: state.albums.unreleasedAlbums,
});

export default connect(
    mapStateToProps,
    { getUnreleasedAlbums }
)(AlbumsFabric);