import * as React from 'react';
import { connect } from 'react-redux';

import { addAlbum } from '../../../../../store/actions';

import mainStyles from "../../../../../styles/main.scss";

interface Props {
    addAlbum: Function;
    close: Function;
    career: any;
}

interface State {
    name: string;
    type: string;
    error: string;
}

class AlbumForm extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            type: 'LP',
            error: '',
        }
    }

    componentDidMount() {

    }

    onChange = (e: any) => {
        this.setState({ [e.currentTarget.name]: e.target.value } as State);
        this.setState({ error: null })
    };

    onSubmit = (e: any) => {
        e.preventDefault();
        this.props.addAlbum(this.props.career.id, this.state).then((res: any) => {
            res.status === "success" ? this.props.close() : this.setState({ error: res.message })
        });
    };

    render() {
        let { name, type, error } = this.state;
        let types = ['LP', 'EP'];

        return <form className="form">
            <div className={`form-group ${mainStyles.formGroup}`}>
                <input value={name} onChange={this.onChange} type="text" name="name" className={`form-control ${mainStyles.formControl}`} placeholder="Название" />
            </div>
            <div className={`form-group ${mainStyles.formGroup}`}>
                <select value={type} onChange={this.onChange} name="type" className={`form-control ${mainStyles.formControl}`} placeholder="Тематика">
                    {types.map((type: string) => {
                        return (
                            <option value={type}>
                                {type}
                            </option>
                        )
                    })}
                </select>
            </div>
            {error && <p className={mainStyles.formError}>{error}</p>}
            <div className={mainStyles.formActions}>
                <a onClick={this.onSubmit} className={`btn btn-primary ${mainStyles.btn} ${mainStyles.btnPrimary}`}>
                    Добавить
                </a>
            </div>
        </form>;
    }
}

const mapStateToProps = (state: any) => ({
    career: state.careers.career,
});

export default connect(
    mapStateToProps,
    { addAlbum }
)(AlbumForm);
