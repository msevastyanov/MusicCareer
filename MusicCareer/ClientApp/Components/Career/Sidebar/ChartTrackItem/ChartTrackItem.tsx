import * as React from 'react';
import moment from "moment";

import styles from "./ChartTrackItem.scss"

interface Props {
    track: any;
}

interface State {

}

class ChartTrackItem extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
    }

    render() {
        let track = this.props.track;

        return <div className={styles.track}>
            <div className="row">
                <div className="col-9">
                    <p className={styles.trackTitle}>
                        {track.name}
                    </p>
                </div>
                <div className="col-3">
                    <span className={styles.trackPosition}>
                        {track.position}
                    </span>
                </div>
            </div>
        </div>;
    }
}

export default ChartTrackItem;
