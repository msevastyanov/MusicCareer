import * as React from 'react';
import moment from "moment";

import styles from "./AlbumItem.scss"

interface Props {
    album: any;
}

interface State {

}

class AlbumItem extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
    }

    getRatingStyle = (rating: number) => {
        let color: string;

        if (rating >= 75)
            color = '#1cc119';
        else if (rating >= 50 && rating < 75)
            color = '#de9d0a';
        else
            color = '#dc3030';

        return {
            color: color
        }
    }

    render() {
        let album = this.props.album;

        return <div className={styles.album}>
            <div className="row">
                <div className="col-2">
                    <span className={styles.albumYear}>
                        {moment(album.releaseDate).format("YYYY")}
                    </span>
                </div>
                <div className="col-7">
                    <p className={styles.albumTitle}>
                        {album.name}
                    </p>
                </div>
                <div className="col-3">
                    <span className={styles.albumRating} style={this.getRatingStyle(album.rating)}>
                        {album.rating}%
                    </span>
                </div>
            </div>
        </div>;
    }
}

export default AlbumItem;
