import * as React from 'react';
import moment from "moment";

import styles from "./CareerItem.scss"

interface Props {
    career: any;
    click: Function;
}

interface State {

}

class CareerItem extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
    }

    render() {
        let career = this.props.career;

        return <div onClick={() => { this.props.click() }} className={styles.careerItem}>
            {career && <span className={styles.careerItemDate}>
                {moment(career.currentDate).format("DD.MM.YYYY")}
            </span>}
            <span className={styles.careerItemName}>
                {career ? career.name : 'Новая карьера'}
            </span>
        </div>;
    }
}

export default CareerItem;
