﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getCareers } from '../../store/actions';
import Modal from "../Modal/Modal"
import CareerForm from "./CareerForm/CareerForm"
import CareerItem from "./CareerItem/CareerItem"

import styles from "./Home.scss"

interface Props {
    getCareers: Function,
    careers: any[],
}

interface State {
    careerModal: boolean;
}

class Home extends React.Component<RouteComponentProps<{}> & Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            careerModal: false,
        }
    }

    componentDidMount() {
        this.props.getCareers()
    }

    open = (careerId: string) => {
        this.props.history.push(`/career/${careerId}`)
    }

    public render() {
        return <div>
            <div className="row">
                <div className="col-md-4 col-lg-3">
                    <CareerItem career={null} click={() => { this.setState({ careerModal: true }) }} />
                </div>
                {this.props.careers.map((career: any) => {
                    return (<div className="col-md-4 col-lg-3">
                        <CareerItem career={career} click={this.open.bind(this, career.id)} />
                    </div>)
                })}
            </div>
            {this.state.careerModal &&
                <Modal title="Новая карьера" close={() => { this.setState({ careerModal: false }) }}>
                    <CareerForm close={() => { this.setState({ careerModal: false }) }} />
                </Modal>}
        </div>;
    }
}

const getCareersProps = (state: any) => {
    console.log('SSSS', state)
    return state.careers.careers;
}

const mapStateToProps = (state: any) => ({
    careers: getCareersProps(state),
});

export default connect(
    mapStateToProps,
    { getCareers }
)(Home);
