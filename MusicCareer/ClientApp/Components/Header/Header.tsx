﻿import React from "react";
import { NavLink } from 'react-router-dom'

import styles from "./Header.scss"

function Header() {
    return (
        <nav id={styles.nav} className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className={`navbar-brand ${styles.navbarBrand}`} href="#">
                Music Career Simulator
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className={`nav-item ${styles.navItem}`}>
                        <NavLink to="/" className="nav-link">
                            Главная
                        </NavLink>
                    </li>
                    <li className={`nav-item ${styles.navItem}`}>
                        <NavLink to="/home" className="nav-link">
                            Карьера
                        </NavLink>
                    </li>
                    <li className={`nav-item ${styles.navItem}`}>
                        <NavLink to="/about" className="nav-link">
                            О проекте
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Header;