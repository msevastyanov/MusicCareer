﻿import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from "../Header/Header"

export interface LayoutProps {
    children?: React.ReactNode;
}

class Layout extends Component<LayoutProps, {}> {
    public render() {
        return <React.Fragment>
            <Header />
            <div className="container-fluid">
                {this.props.children}
            </div>
        </React.Fragment>;
    }
}

export default Layout;