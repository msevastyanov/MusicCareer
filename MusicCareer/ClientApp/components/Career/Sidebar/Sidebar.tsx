﻿import * as React from 'react';
import { connect } from 'react-redux';
import CareerModel from "../../../models/career";
import AlbumItem from "./AlbumItem/AlbumItem"
import ChartTrackItem from "./ChartTrackItem/ChartTrackItem"

import mainStyles from "../../../styles/main.scss";
import styles from "./Sidebar.scss";

interface Props {
    career: any;
    releasedAlbums: any[];
}

interface State {

}

class Sidebar extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
    }

    render() {
        let career = new CareerModel(this.props.career);

        let albums = this.props.releasedAlbums;

        let chartTracks = [{
            name: 'My Name Is',
            position: 46
        }, {
            name: 'Till I Collapse',
            position: 32
        }, {
            name: 'Venom',
            position: 87
        }, {
            name: 'Lose Yourself',
            position: 12
        }]

        return <div className={`${mainStyles.card} ${styles.sidebar}`}>
            <p className={styles.artistName}>
                {career.name}
            </p>
            <div className={mainStyles.separator}></div>
            <div className={styles.careerPower}>
                <div className="row">
                    <div className="col-sm-8">
                        Подписчики:
                    </div>
                    <div className="col-sm-4">
                        {career.subscribers}
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-8">
                        Интерес:
                    </div>
                    <div className="col-sm-4">
                        {career.hype}%
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-8">
                        Энергия:
                    </div>
                    <div className="col-sm-4">
                        {career.energy}%
                    </div>
                </div>
            </div>
            <div className={mainStyles.separator}></div>
            <div className={styles.discography}>
                {albums.map((album: any, index: number) => <AlbumItem key={index} album={album} />)}
                {!albums.length && <p className={mainStyles.info}>Нет выпущенных альбомов</p>}
            </div>
            <div className={mainStyles.separator}></div>
            <div className={styles.careerCharts}>
                {chartTracks.sort((a: any, b: any) => a.position - b.position).map((track: any, index: number) => <ChartTrackItem key={index} track={track} />)}
            </div>
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
    career: state.careers.career,
    releasedAlbums: state.albums.releasedAlbums,
});

export default connect(
    mapStateToProps,
    { }
)(Sidebar);