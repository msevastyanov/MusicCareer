﻿import * as React from 'react';
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux';
import CareerModel from "../../../models/career";

import mainStyles from "../../../styles/main.scss";
import styles from "./Topbar.scss";

interface Props {
    career: any;
}

interface State {

}

class Topbar extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
    }

    render() {
        let career = new CareerModel(this.props.career)
        let id = career.id;

        return <div className={`${mainStyles.card} ${styles.topbar}`}>
            <NavLink to={`/career/${id}`} className={styles.topbarLink}>
                Лента
            </NavLink>
            <NavLink to={`/career/${id}/discography`} className={styles.topbarLink}>
                Дискография
            </NavLink>
            <NavLink to={`/career/${id}/achievements`} className={styles.topbarLink}>
                Достижения
            </NavLink>
            <NavLink to={`/career/${id}/fabric`} className={styles.topbarLink}>
                Создание
            </NavLink>
        </div>;
    }
}

const mapStateToProps = (state: any) => ({
    career: state.careers.career,
});

export default connect(
    mapStateToProps,
    {}
)(Topbar);