﻿import * as React from 'react';
import { connect } from 'react-redux';

import { addCareer } from '../../../store/actions';

import mainStyles from "../../../styles/main.scss";

interface Props {
    addCareer: Function;
    close: Function;
}

interface State {
    name: string;
    error: string;
}

class CareerFrom extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            error: '',
        }
    }

    componentDidMount() {

    }

    onChange = (e: any) => {
        this.setState({ [e.currentTarget.name]: e.target.value } as State);
        this.setState({error: null})
    };

    onSubmit = (e: any) => {
        e.preventDefault();
        this.props.addCareer(this.state).then((res: any) => {
            console.log('resssss', res)
            res.status === "success" ? this.props.close() : this.setState({error: res.message})
        });
    };

    render() {
        let { name, error } = this.state;

        return <form className="form">
            <div className={`form-group ${mainStyles.formGroup}`}>
                <input value={name} onChange={this.onChange} type="text" name="name" className={`form-control ${mainStyles.formControl}`} placeholder="Никнейм" />
            </div>
            {error && <p className={mainStyles.formError}>{error}</p>}
            <div className={mainStyles.formActions}>
                <a onClick={this.onSubmit} className={`btn btn-primary ${mainStyles.btn} ${mainStyles.btnPrimary}`}>
                    Добавить
                </a>
            </div>
        </form>;
    }
}

const mapStateToProps = () => ({
    
});

export default connect(
    mapStateToProps,
    { addCareer }
)(CareerFrom);

