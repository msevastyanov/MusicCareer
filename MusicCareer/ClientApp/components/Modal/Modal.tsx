﻿import * as React from 'react';

import styles from "./Modal.scss"

interface Props {
    title: string;
    children: any;
    close: Function;
}

interface State {

}

class Modal extends React.Component<Props, State> {
    node: any;

    constructor(props: any) {
        super(props);
    }

    componentWillMount() {
        document.addEventListener('mousedown', this.handleClick, false);
        document.addEventListener("keydown", this.handleEsc, false);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClick, false);
        document.removeEventListener("keydown", this.handleEsc, false);
    }

    handleEsc = (e: any) => { 
        if (e.keyCode === 27) {
            this.close()
        }
    }

    handleClick = (e: any) => {
        if (!this.node.contains(e.target)) {
            this.close()
        }
    }

    close = () => {
        this.props.close()
    }

    render() {
        return <div className={styles.modalWrapper}>
            <div ref={node => this.node = node} className={styles.modal}>
                <a onClick={this.close.bind(this)} className={styles.modalClose}>
                    <i className="fas fa-times"></i>
                </a>
                <div className={styles.modalTitle}>
                    {this.props.title}
                </div>
                <div className={styles.modalContent}>
                    {this.props.children}
                </div>
            </div>
        </div>;
    }
}

export default Modal;
