﻿import * as React from 'react';
import { Route } from 'react-router-dom';
import Layout from './components/Layout/Layout';
import Home from './components/Home/Home';
import Index from './components/Index/Index';
import Career from './components/Career/Career';

export const routes = <Layout>
    <Route exact path='/' component={Index} />
    <Route exact path='/home' component={Home} />
    <Route path="/career/:id" component={Career} />
</Layout>;
