﻿using MusicCareer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.ViewModels
{
    public class TrackInfoViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int? ChartsPeek { get; set; }
        public string Album { get; set; }
        public Guid? AlbumId { get; set; }
        public bool IsSingle { get; set; }
        public bool IsReleased { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string Theme { get; set; }
        public string Style { get; set; }
        public DateTime? RecordedDate { get; set; }
        public TrackInfoViewModel(Track track)
        {
            Id = track.Id;
            Name = track.Name;
            ChartsPeek = track.ChartsPeek;
            Album = track.Album?.Name;
            AlbumId = track.Album?.Id;
            IsSingle = track.IsSingle;
            IsReleased = track.IsReleased;
            ReleaseDate = track.ReleaseDate;
            Theme = track.Theme;
            Style = track.Style;
            RecordedDate = track.RecordedDate;
        }
    }

    public class TrackCreateViewModel
    {
        public string Name { get; set; }
        public string Theme { get; set; }
        public string Style { get; set; }
    }
}
