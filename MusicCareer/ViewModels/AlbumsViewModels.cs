﻿using MusicCareer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.ViewModels
{
    public class AlbumInfoViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Guid> Tracks { get; set; }
        public string Type { get; set; }
        public bool IsReleased { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int? Rating { get; set; }
        public AlbumInfoViewModel(Album album)
        {
            Id = album.Id;
            Name = album.Name;
            Tracks = album.Tracks.Select(item => item.Id).ToList();
            Type = album.Type.ToString();
            IsReleased = album.IsReleased;
            ReleaseDate = album.ReleaseDate;
            Rating = album.Rating;
        }
    }

    public class AlbumCreateViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class AlbumUpdateViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}

