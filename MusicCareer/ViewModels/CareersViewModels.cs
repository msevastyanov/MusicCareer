﻿using MusicCareer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.ViewModels
{
    public class CareerCreateViewModel
    {
        [Required]
        public string Name { get; set; }
    }

    public class CareerUpdateViewModel
    {
        [Required]
        public string Name { get; set; }
    }

    public class CareerInfoViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CurrentDate { get; set; }
        public int Subscribers { get; set; }
        public int Hype { get; set; }
        public int Energy { get; set; }

        public CareerInfoViewModel(Career career)
        {
            Id = career.Id;
            Name = career.Name;
            CurrentDate = career.CurrentDate;
            Subscribers = career.Subscribers;
            Hype = career.Hype;
            Energy = career.Energy;
        }
    }
}
