using System;
using System.Linq;
using MusicCareer.Models;

namespace MusicCareer.Simulation
{
    public static class AlbumReleaseSimulation
    {
        static Random rnd = new Random();

        public static int GetTotalRating(Album album)
        {
            int mulct = 0;

            for (var i = 0; i < 3; i++)
            {
                mulct += GetRatingFromReviewer(album);
            }

            return mulct / 3;
        }

        public static int GetRatingFromReviewer(Album album)
        {
            int mulct = 0;

            foreach (var theme in ThemesAnalyzer.Themes)
            {
                var tracks = album.Tracks.Where(t => t.Theme == theme.Name).ToList();
                if (tracks.Count > theme.Count)
                {
                    var themeMulct = rnd.Next(theme.MinMulct, theme.MaxMulct);
                    mulct += themeMulct;
                }
            }

            var originalMulct = rnd.Next(0, 15);
            mulct += originalMulct;

            var qualityMulct = rnd.Next(0, 15);
            mulct += qualityMulct;

            return 100 - mulct;
        }
    }
}
