using System;
using System.Collections.Generic;
using MusicCareer.Models;

namespace MusicCareer.Simulation
{
    public static class ThemesAnalyzer
    {
        public static List<Theme> Themes = new List<Theme> {
            new Theme {
                Name = "me",
                Count = 5,
                MinMulct = 3,
                MaxMulct = 7
            },
            new Theme {
                Name = "story",
                Count = 5,
                MinMulct = 0,
                MaxMulct = 3
            },
            new Theme {
                Name = "love",
                Count = 5,
                MinMulct = 5,
                MaxMulct = 10
            },
            new Theme {
                Name = "sex",
                Count = 3,
                MinMulct = 7,
                MaxMulct = 13
            },
            new Theme {
                Name = "death",
                Count = 5,
                MinMulct = 4,
                MaxMulct = 8
            },
            new Theme {
                Name = "money",
                Count = 4,
                MinMulct = 6,
                MaxMulct = 15
            },
            new Theme {
                Name = "social",
                Count = 7,
                MinMulct = 5,
                MaxMulct = 15
            },
            new Theme {
                Name = "politics",
                Count = 5,
                MinMulct = 7,
                MaxMulct = 20
            },
            new Theme {
                Name = "drugs",
                Count = 3,
                MinMulct = 5,
                MaxMulct = 17
            },
            new Theme {
                Name = "motivation",
                Count = 7,
                MinMulct = 0,
                MaxMulct = 5
            },
            new Theme {
                Name = "philosophy",
                Count = 7,
                MinMulct = 3,
                MaxMulct = 7
            },
            new Theme {
                Name = "music",
                Count = 3,
                MinMulct = 0,
                MaxMulct = 7
            },
            new Theme {
                Name = "family",
                Count = 3,
                MinMulct = 0,
                MaxMulct = 7
            },
            new Theme {
                Name = "nostalgia",
                Count = 3,
                MinMulct = 0,
                MaxMulct = 5
            }
        };
    }
    public class Theme
    {
        public string Name;
        public int Count;
        public int MinMulct;
        public int MaxMulct;
    }
}
