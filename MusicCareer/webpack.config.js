﻿const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: { main: './ClientApp/app.tsx' },
    output: {
        path: path.resolve(__dirname, './wwwroot/dist'),
        filename: 'bundle.js',
        publicPath: 'dist/'
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            {
                test: /\.(js|jsx)/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets": ["@babel/preset-env", "@babel/preset-react"]
                    }
                }
            },
            {
                test: /\.(css|scss)$/,
                exclude: /node_modules/,
                use: [
                    'css-hot-loader',
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            camelCase: true,
                            modules: true,
                            importLoaders: 1,
                            sourceMap: true
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            camelCase: true,
                            modules: true,
                            importLoaders: 1,
                            sourceMap: true
                        },
                    },
                ],
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000'
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
    ],
    resolve: {
        alias: { 'react-dom': '@hot-loader/react-dom' },
        modules: [
            path.resolve(__dirname, "/ClientApp"),
            path.resolve(__dirname, "node_modules/")
        ],
        extensions: ['*', '.js', '.jsx', '.ts', '.tsx', '.css', '.scss']
    },
    node: {
        process: false,
        global: false,
        fs: "empty"
    }
};