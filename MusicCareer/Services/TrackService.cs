﻿using Microsoft.EntityFrameworkCore;
using MusicCareer.Data;
using MusicCareer.Models;
using MusicCareer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Services
{
    public class TrackService : ITrackService
    {
        private readonly ApplicationDbContext _db;

        public TrackService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<TrackInfoViewModel>> GetAllTracks(Guid careerId)
        {
            return await _db.Tracks
                .Include(t => t.Album)
                .Include(t => t.Career)
                .Where(t => t.Career.Id == careerId)
                .Select(item => new TrackInfoViewModel(item))
                .ToListAsync();
        }

        public async Task<List<TrackInfoViewModel>> GetUnreleasedTracks(Guid careerId)
        {
            return await _db.Tracks
                .Include(t => t.Album)
                .Include(t => t.Career)
                .Where(t => t.Career.Id == careerId && !t.IsReleased && (t.Album == null || !t.Album.IsReleased))
                .Select(item => new TrackInfoViewModel(item))
                .ToListAsync();
        }

        public async Task<List<TrackInfoViewModel>> GetTracksByAlbum(Guid albumId)
        {
            return await _db.Tracks
                .Include(t => t.Album)
                .Where(t => t.Album.Id == albumId)
                .Select(item => new TrackInfoViewModel(item))
                .ToListAsync();
        }

        public async Task<RequestResult<TrackInfoViewModel>> CreateTrack(TrackCreateViewModel model, Guid careerId)
        {
            var existingTrack = await _db.Tracks.Include(item => item.Career).FirstOrDefaultAsync(item => item.Name == model.Name && item.Career.Id == careerId);
            if (existingTrack != null)
                return RequestResult<TrackInfoViewModel>.Failed("Трек с таким названием уже существует");

            var career = await _db.Careers.SingleOrDefaultAsync(item => item.Id == careerId);
            if (career == null)
                return RequestResult<TrackInfoViewModel>.Failed("Неверный ID карьеры");

            if (career.Energy < 30)
                return RequestResult<TrackInfoViewModel>.Failed("Недостаточно энергии");

            career.WriteTrack();

            var track = new Track
            {
                Name = model.Name,
                Theme = model.Theme,
                Style = model.Style ?? "-",
                IsSingle = false,
                IsReleased = false,
                ReleaseDate = null,
                ChartsPeek = null,
                Career = career,
                RecordedDate = career.CurrentDate,
                CreatedDate = DateTime.UtcNow,
                ChangedDate = DateTime.UtcNow,
            };

            _db.Tracks.Add(track);
            await _db.SaveChangesAsync();

            return RequestResult<TrackInfoViewModel>.Success(new TrackInfoViewModel(track));
        }

        public async Task<RequestResult<TrackInfoViewModel>> RemoveTrack(Guid id)
        {
            var track = await _db.Tracks.SingleOrDefaultAsync(item => item.Id == id);
            if (track == null)
                return RequestResult<TrackInfoViewModel>.Failed("Неверный ID трека");
                
            if (track.IsReleased || (track.Album != null && track.Album.IsReleased))
                return RequestResult<TrackInfoViewModel>.Failed("Трек уже выпущен");

            _db.Tracks.Remove(track);

            await _db.SaveChangesAsync();

            return RequestResult<TrackInfoViewModel>.Success(new TrackInfoViewModel(track));
        }

        public async Task<RequestResult<TrackInfoViewModel>> ReleaseTrack(Guid careerId, Guid trackId)
        {
            var track = await _db.Tracks.Include(t => t.Album).SingleOrDefaultAsync(item => item.Id == trackId);
            if (track == null)
                return RequestResult<TrackInfoViewModel>.Failed("Неверный ID трека");

            var career = await _db.Careers.SingleOrDefaultAsync(item => item.Id == careerId);
            if (career == null)
                return RequestResult<TrackInfoViewModel>.Failed("Неверный ID карьеры");

            track.IsReleased = true;
            track.ReleaseDate = career.CurrentDate;

            await _db.SaveChangesAsync();

            return RequestResult<TrackInfoViewModel>.Success(new TrackInfoViewModel(track));
        }
    }
}
