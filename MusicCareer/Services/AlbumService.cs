﻿using Microsoft.EntityFrameworkCore;
using MusicCareer.Data;
using MusicCareer.Models;
using MusicCareer.Simulation;
using MusicCareer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Services
{
    public class AlbumService : IAlbumService
    {
        private readonly ApplicationDbContext _db;

        public AlbumService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<AlbumInfoViewModel>> GetAlbums(Guid careerId, bool isReleased)
        {
            return await _db.Albums
                .Include(item => item.Tracks)
                .Include(item => item.Career)
                .Where(item => item.Career.Id == careerId && item.IsReleased == isReleased)
                .Select(item => new AlbumInfoViewModel(item))
                .ToListAsync();
        }

        public async Task<RequestResult<AlbumInfoViewModel>> CreateAlbum(AlbumCreateViewModel model, Guid careerId)
        {
            var existingAlbum = await _db.Albums.Include(item => item.Career).FirstOrDefaultAsync(item => item.Name == model.Name && item.Career.Id == careerId);
            if (existingAlbum != null)
                return RequestResult<AlbumInfoViewModel>.Failed("Альбом с таким именем уже существует");

            var career = await _db.Careers.SingleOrDefaultAsync(item => item.Id == careerId);
            if (career == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID карьеры");

            var album = new Album
            {
                Name = model.Name,
                IsReleased = false,
                ReleaseDate = null,
                Tracks = new List<Track>(),
                Type = model.Type,
                Career = career,
                Rating = 0,
                CreatedDate = DateTime.UtcNow,
                ChangedDate = DateTime.UtcNow,
            };

            _db.Albums.Add(album);
            await _db.SaveChangesAsync();

            return RequestResult<AlbumInfoViewModel>.Success(new AlbumInfoViewModel(album));
        }

        public async Task<RequestResult<AlbumInfoViewModel>> UpdateAlbum(Guid id, AlbumUpdateViewModel model)
        {
            var album = await _db.Albums.SingleOrDefaultAsync(item => item.Id == id);
            if (album == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID альбома");

            album.Name = model.Name;
            album.Type = model.Type;
            album.ChangedDate = DateTime.UtcNow;

            await _db.SaveChangesAsync();
            return RequestResult<AlbumInfoViewModel>.Success(new AlbumInfoViewModel(album));
        }

        public async Task<RequestResult<AlbumInfoViewModel>> RemoveAlbum(Guid id)
        {
            var album = await _db.Albums.Include(item => item.Tracks).SingleOrDefaultAsync(item => item.Id == id);
            if (album == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID альбома");

            if (album.IsReleased)
                return RequestResult<AlbumInfoViewModel>.Failed("Альбом уже выпущен");

            _db.Albums.Remove(album);

            await _db.SaveChangesAsync();

            return RequestResult<AlbumInfoViewModel>.Success(new AlbumInfoViewModel(album));
        }

        public async Task<RequestResult<AlbumInfoViewModel>> ReleaseAlbum(Guid careerId, Guid albumId)
        {
            var album = await _db.Albums.Include(item => item.Tracks).SingleOrDefaultAsync(item => item.Id == albumId);
            if (album == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID альбома");
            if (album.IsReleased)
                return RequestResult<AlbumInfoViewModel>.Failed("Альбом уже выпущен");

            var career = await _db.Careers.SingleOrDefaultAsync(item => item.Id == careerId);
            if (career == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID карьеры");
            if (career.Energy < 70)
                return RequestResult<AlbumInfoViewModel>.Failed("Недостаточно энергии");

            album.IsReleased = true;
            album.ReleaseDate = career.CurrentDate;
            album.Rating = AlbumReleaseSimulation.GetTotalRating(album);

            foreach (var track in album.Tracks) {
                track.IsReleased = true;
                track.ReleaseDate = career.CurrentDate;
            }

            await _db.SaveChangesAsync();

            return RequestResult<AlbumInfoViewModel>.Success(new AlbumInfoViewModel(album));
        }

        public async Task<RequestResult<AlbumInfoViewModel>> AddTrackToAlbum(Guid albumId, Guid trackId)
        {
            var album = await _db.Albums.Include(item => item.Tracks).SingleOrDefaultAsync(item => item.Id == albumId);
            if (album == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID альбома");;
            if (album.IsReleased)
                return RequestResult<AlbumInfoViewModel>.Failed("Альбом уже выпущен");

            var track = await _db.Tracks.SingleOrDefaultAsync(item => item.Id == trackId);
            if (track == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID трека");
            if (track.IsReleased)
                return RequestResult<AlbumInfoViewModel>.Failed("Трек уже выпущен");

            album.Tracks.Add(track);

            await _db.SaveChangesAsync();

            return RequestResult<AlbumInfoViewModel>.Success(new AlbumInfoViewModel(album));
        }

        public async Task<RequestResult<AlbumInfoViewModel>> RemoveTrackFromAlbum(Guid albumId, Guid trackId)
        {
            var album = await _db.Albums.Include(item => item.Tracks).SingleOrDefaultAsync(item => item.Id == albumId);
            if (album == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID альбома");

            var track = await _db.Tracks.Include(item => item.Album).SingleOrDefaultAsync(item => item.Id == trackId);
            if (track == null)
                return RequestResult<AlbumInfoViewModel>.Failed("Неверный ID трека");

            track.Album = null;

            await _db.SaveChangesAsync();

            return RequestResult<AlbumInfoViewModel>.Success(new AlbumInfoViewModel(album));
        }
    }
}
  