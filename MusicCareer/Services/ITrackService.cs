﻿using MusicCareer.Models;
using MusicCareer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Services
{
    public interface ITrackService
    {
        Task<List<TrackInfoViewModel>> GetAllTracks(Guid careerId);
        Task<List<TrackInfoViewModel>> GetUnreleasedTracks(Guid careerId);
        Task<List<TrackInfoViewModel>> GetTracksByAlbum(Guid albumId);
        Task<RequestResult<TrackInfoViewModel>> CreateTrack(TrackCreateViewModel model, Guid careerId);
        Task<RequestResult<TrackInfoViewModel>> RemoveTrack(Guid id);
        Task<RequestResult<TrackInfoViewModel>> ReleaseTrack(Guid careerId, Guid trackId);
    }
}
