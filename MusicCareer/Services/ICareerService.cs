﻿using MusicCareer.Models;
using MusicCareer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Services
{
    public interface ICareerService
    {
        Task<List<CareerInfoViewModel>> GetCareers();
        Task<CareerInfoViewModel> GetCareer(Guid careerId);
        Task<RequestResult<CareerInfoViewModel>> CreateCareer(CareerCreateViewModel model);
        Task<RequestResult<CareerInfoViewModel>> UpdateCareer(Guid id, CareerUpdateViewModel model);
        Task<RequestResult<CareerInfoViewModel>> RemoveCareer(Guid id);
        Task<RequestResult<CareerInfoViewModel>> NextWeek(Guid id);
    }
}
