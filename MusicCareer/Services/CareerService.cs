﻿using Microsoft.EntityFrameworkCore;
using MusicCareer.Data;
using MusicCareer.Models;
using MusicCareer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Services
{
    public class CareerService : ICareerService
    {
        private readonly ApplicationDbContext _db;

        public CareerService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<CareerInfoViewModel>> GetCareers()
        {
            return await _db.Careers
                .Select(item => new CareerInfoViewModel(item))
                .ToListAsync();
        }

        public async Task<CareerInfoViewModel> GetCareer(Guid careerId)
        {
            var career = await _db.Careers.FirstOrDefaultAsync(item => item.Id == careerId);
            return new CareerInfoViewModel(career);
        }

        public async Task<RequestResult<CareerInfoViewModel>> CreateCareer(CareerCreateViewModel model)
        {
            var existingCareer = await _db.Careers.FirstOrDefaultAsync(item => item.Name == model.Name);
            if (existingCareer != null)
                return RequestResult<CareerInfoViewModel>.Failed("Карьера с таким именем уже существует");

            var career = new Career
            {
                Name = model.Name,
                Subscribers = 0,
                Hype = 0,
                Energy = 100,
                CreatedDate = DateTime.UtcNow,
                ChangedDate = DateTime.UtcNow,
                CurrentDate = new DateTime(2006, 1, 1)
            };

            _db.Careers.Add(career);
            await _db.SaveChangesAsync();

            return RequestResult<CareerInfoViewModel>.Success(new CareerInfoViewModel(career));
        }

        public async Task<RequestResult<CareerInfoViewModel>> UpdateCareer(Guid id, CareerUpdateViewModel model)
        {
            var career = await _db.Careers.SingleOrDefaultAsync(item => item.Id == id);
            if (career == null)
                return RequestResult<CareerInfoViewModel>.Failed("Неверный ID карьеры");

            career.Name = model.Name;
            career.ChangedDate = DateTime.UtcNow;

            await _db.SaveChangesAsync();
            return RequestResult<CareerInfoViewModel>.Success(new CareerInfoViewModel(career));
        }

        public async Task<RequestResult<CareerInfoViewModel>> RemoveCareer(Guid id)
        {
            var career = await _db.Careers.SingleOrDefaultAsync(item => item.Id == id);
            if (career == null)
                return RequestResult<CareerInfoViewModel>.Failed("Неверный ID карьеры");

            _db.Careers.Remove(career);

            await _db.SaveChangesAsync();

            return RequestResult<CareerInfoViewModel>.Success(new CareerInfoViewModel(career));
        }

        public async Task<RequestResult<CareerInfoViewModel>> NextWeek(Guid id)
        {
            var career = await _db.Careers.SingleOrDefaultAsync(item => item.Id == id);
            if (career == null)
                return RequestResult<CareerInfoViewModel>.Failed("Неверный ID карьеры");

            career.NextWeek();

            await _db.SaveChangesAsync();

            return RequestResult<CareerInfoViewModel>.Success(new CareerInfoViewModel(career));
        }
    }
}
