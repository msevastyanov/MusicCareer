﻿using MusicCareer.Models;
using MusicCareer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicCareer.Services
{
    public interface IAlbumService
    {
        Task<List<AlbumInfoViewModel>> GetAlbums(Guid careerId, bool isReleased);
        Task<RequestResult<AlbumInfoViewModel>> CreateAlbum(AlbumCreateViewModel model, Guid careerId);
        Task<RequestResult<AlbumInfoViewModel>> UpdateAlbum(Guid id, AlbumUpdateViewModel model);
        Task<RequestResult<AlbumInfoViewModel>> RemoveAlbum(Guid id);
        Task<RequestResult<AlbumInfoViewModel>> ReleaseAlbum(Guid careerId, Guid albumId);
        Task<RequestResult<AlbumInfoViewModel>> AddTrackToAlbum(Guid albumId, Guid trackId);
        Task<RequestResult<AlbumInfoViewModel>> RemoveTrackFromAlbum(Guid albumId, Guid trackId);
    }
}
