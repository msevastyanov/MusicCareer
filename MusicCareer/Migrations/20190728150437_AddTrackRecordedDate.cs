﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicCareer.Migrations
{
    public partial class AddTrackRecordedDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ChartsPosition",
                table: "Tracks",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LastChartsPosition",
                table: "Tracks",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RecordedDate",
                table: "Tracks",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChartsPosition",
                table: "Tracks");

            migrationBuilder.DropColumn(
                name: "LastChartsPosition",
                table: "Tracks");

            migrationBuilder.DropColumn(
                name: "RecordedDate",
                table: "Tracks");
        }
    }
}
